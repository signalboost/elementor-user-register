#Elementor User Register form
Custom register form widget for Elementor.

##Requires
Elementor 2.4.0
PHP 7.0 or greater

##Futures
If akismet is installed, it will check all users thru the spam filter, and if it is detected as spam. It will throw an error

WooCommerce support for give users an free product on register

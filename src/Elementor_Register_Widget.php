<?php

use Elementor\Widget_Base;

if (!defined('ABSPATH')) exit;

class Elementor_Register_Widget extends Widget_Base
{
    public function get_name()
    {
        return 'user_register';
    }

    public function get_title()
    {
        return __('User Register', 'sb-elementor');
    }

    public function get_icon()
    {
        return 'fa fa-users';
    }

    public function get_categories()
    {
        return ['general'];
    }

    protected function _register_controls()
    {
        $this->start_controls_section(
            'form_settings_section',
            [
                'label' => __('Form Fields', 'sb-elementor'),
                'tab'   => \Elementor\Controls_Manager::TAB_SETTINGS,
            ]
        );
        $this->add_control(
            'show_labels',
            [
                'label'        => __('Labels', 'sb-elementor'),
                'type'         => \Elementor\Controls_Manager::SWITCHER,
                'label_on'     => __('Show', 'sb-elementor'),
                'label_off'    => __('Hide', 'sb-elementor'),
                'return_value' => 'yes',
                'default'      => 'yes',
            ]
        );
        $this->add_control(
            'display_name',
            [
                'label'        => __('Show Username', 'sb-elementor'),
                'type'         => \Elementor\Controls_Manager::SWITCHER,
                'label_on'     => __('Show', 'sb-elementor'),
                'label_off'    => __('Hide', 'sb-elementor'),
                'return_value' => 'yes',
                'default'      => 'yes',
            ]
        );
        $this->add_control(
            'auto_username',
            [
                'label'        => __('Generate username with 9 digits', 'sb-elementor'),
                'type'         => \Elementor\Controls_Manager::SWITCHER,
                'label_on'     => __('Yes', 'sb-elementor'),
                'label_off'    => __('No', 'sb-elementor'),
                'return_value' => 'yes',
                'default'      => 'yes',
            ]
        );
        $this->add_control(
            'show_password',
            [
                'label'        => __('Show password field, or auto generate password', 'sb-elementor'),
                'type'         => \Elementor\Controls_Manager::SWITCHER,
                'label_on'     => __('Show', 'sb-elementor'),
                'label_off'    => __('Hide', 'sb-elementor'),
                'return_value' => 'yes',
                'default'      => 'yes',
            ]
        );
        /*$this->add_control(
            'show_newsletter',
            [
                'label'        => __('Show newsletter checkbox', 'sb-elementor'),
                'type'         => \Elementor\Controls_Manager::SWITCHER,
                'label_on'     => __('Show', 'sb-elementor'),
                'label_off'    => __('Hide', 'sb-elementor'),
                'return_value' => 'yes',
                'default'      => 'yes',
            ]
        );*/
        if (class_exists('WooCommerce')) {
            $this->add_control(
                'enable_woo_fields',
                [
                    'label'        => __('Enable WooCommerce fields', 'sb-elementor'),
                    'type'         => \Elementor\Controls_Manager::SWITCHER,
                    'label_on'     => __('Yes', 'sb-elementor'),
                    'label_off'    => __('No', 'sb-elementor'),
                    'return_value' => 'yes',
                    'default'      => 'yes',
                ]
            );
            $this->add_control(
                'company_show',
                [
                    'label'        => __('Show company name field', 'sb-elementor'),
                    'type'         => \Elementor\Controls_Manager::SWITCHER,
                    'label_on'     => __('Yes', 'sb-elementor'),
                    'label_off'    => __('No', 'sb-elementor'),
                    'return_value' => 'yes',
                    'default'      => 'no',
                    'condition'    => [
                        'enable_woo_fields' => 'yes',
                    ],
                ]
            );
            $this->add_control(
                'address2_show',
                [
                    'label'        => __('Show Address2 field', 'sb-elementor'),
                    'type'         => \Elementor\Controls_Manager::SWITCHER,
                    'label_on'     => __('Yes', 'sb-elementor'),
                    'label_off'    => __('No', 'sb-elementor'),
                    'return_value' => 'yes',
                    'default'      => 'no',
                    'condition'    => [
                        'enable_woo_fields' => 'yes',
                    ],
                ]
            );
            $this->add_control(
                'phone_show',
                [
                    'label'        => __('Show Phone field', 'sb-elementor'),
                    'type'         => \Elementor\Controls_Manager::SWITCHER,
                    'label_on'     => __('Yes', 'sb-elementor'),
                    'label_off'    => __('No', 'sb-elementor'),
                    'return_value' => 'yes',
                    'default'      => 'no',
                    'condition'    => [
                        'enable_woo_fields' => 'yes',
                    ],
                ]
            );
            $this->add_control(
                'enable_free_product',
                [
                    'label'        => __('Enable free Product', 'sb-elementor'),
                    'type'         => \Elementor\Controls_Manager::SWITCHER,
                    'label_on'     => __('Yes', 'sb-elementor'),
                    'label_off'    => __('No', 'sb-elementor'),
                    'return_value' => 'yes',
                    'default'      => 'no',
                    'condition'    => [
                        'enable_woo_fields' => 'yes',
                    ],
                ]
            );
            $this->add_control(
                'enable_free_product_text',
                [
                    'label'        => __('Show text for free product', 'sb-elementor'),
                    'type'         => \Elementor\Controls_Manager::SWITCHER,
                    'label_on'     => __('Yes', 'sb-elementor'),
                    'label_off'    => __('No', 'sb-elementor'),
                    'return_value' => 'yes',
                    'default'      => 'no',
                    'condition'    => [
                        'enable_free_product' => 'yes',
                    ],
                ]
            );
        };
        $this->end_controls_section();
        $this->start_controls_section(
            'form_content_section',
            [
                'label' => __('Form Labels', 'sb-elementor'),
                'tab'   => \Elementor\Controls_Manager::TAB_SETTINGS,
            ]
        );

        $this->add_control(
            'display_name_label',
            [
                'label'     => __('Username Label', 'sb-elementor'),
                'type'      => \Elementor\Controls_Manager::TEXT,
                'default'   => __('Username', 'sb-elementor'),
                'condition' => [
                    'display_name' => 'yes',
                ],
            ]
        );
        $this->add_control(
            'password_label',
            [
                'label'     => __('Password Label', 'sb-elementor'),
                'type'      => \Elementor\Controls_Manager::TEXT,
                'default'   => __('Password', 'sb-elementor'),
                'condition' => [
                    'show_password' => 'yes',
                ],
            ]
        );
        $this->add_control(
            're_password_label',
            [
                'label'     => __('Reenter Password Label', 'sb-elementor'),
                'type'      => \Elementor\Controls_Manager::TEXT,
                'default'   => __('Reenter Password', 'sb-elementor'),
                'condition' => [
                    'show_password' => 'yes',
                ],
            ]
        );
        $this->add_control(
            'newsletter_label',
            [
                'label'     => __('Newsletter Label', 'sb-elementor'),
                'type'      => \Elementor\Controls_Manager::TEXT,
                'default'   => __(' Subscribe me to the Newsletter', 'sb-elementor'),
                'condition' => [
                    'show_newsletter' => 'yes',
                ],
            ]
        );
        $this->end_controls_section();

        $this->start_controls_section(
                'form_woo_settings',
                [
                    'label' => __('Free Product settings', 'sb-elementor'),
                    'tab' => \Elementor\Controls_Manager::TAB_SETTINGS,
                    'condition'    => [
                        'enable_free_product' => 'yes',
                    ],
                ]
        );
        $this->add_control(
            'free_product_title',
            [
                'label'     => __('Free product title', 'sb-elementor'),
                'type'      => \Elementor\Controls_Manager::TEXT,
                'default'   => __("First One's On ", 'sb-elementor') . get_bloginfo('name'),
                'condition' => [
                    'enable_free_product' => 'yes',
                ],
            ]
        );
        $this->add_control(
            'free_product_info',
            [
                'label'     => __('Free product info', 'sb-elementor'),
                'type'      => \Elementor\Controls_Manager::TEXTAREA,
                'default'   => __("We would like to gift you your first print. Your information stays secret.", 'sb-elementor'),
                'condition' => [
                    'enable_free_product' => 'yes',
                ],
            ]
        );
        $this->add_control(
                'products',
                [
                    'label' => __('Products'),
                    'type' => \Elementor\Controls_Manager::SELECT2,
                    'multiple' => true,
                    'options' => $this->getAllProducts()
                ]
        );
        $this->end_controls_section();

        $this->start_controls_section(
            'form_uri_section',
            [
                'label' => __('Form Privacy URL', 'sb-elementor'),
                'tab'   => \Elementor\Controls_Manager::TAB_SETTINGS,
            ]
        );
        $this->add_control(
            'dmca_url',
            [
                'label' => __('DMCA URL', 'sb-elementor'),
                'type'  => \Elementor\Controls_Manager::URL,
            ]
        );
        $this->add_control(
            'terms_url',
            [
                'label' => __('Terms & Conditions', 'sb-elementor'),
                'type'  => \Elementor\Controls_Manager::URL,
            ]
        );
        $this->add_control(
            'privacy_url',
            [
                'label' => __('Privacy Policy', 'sb-elementor'),
                'type'  => \Elementor\Controls_Manager::URL,
            ]
        );
        $this->end_controls_section();

        $this->start_controls_section(
            'form_style',
            [
                'label' => __('Form style', 'sb-elementor'),
                'tab'   => \Elementor\Controls_Manager::TAB_STYLE,
            ]
        );
        $this->add_control(
            'form_width',
            [
                'label'      => __('Form width', 'sb-elementor'),
                'type'       => \Elementor\Controls_Manager::SLIDER,
                'size_units' => ['px', '%'],
                'range'      => [
                    'px' => [
                        'min'  => 500,
                        'max'  => 1200,
                        'step' => 100,
                    ],
                    '%'  => [
                        'min' => 30,
                        'max' => 100,
                    ],
                ],
                'default'    => [
                    'unit' => '%',
                    'size' => 100,
                ],
            ]
        );
        $this->add_control(
            'row_gap',
            [
                'label'      => __('Row gap', 'sb-elementor'),
                'type'       => \Elementor\Controls_Manager::SLIDER,
                'size_units' => ['px'],
                'range'      => [
                    'px' => [
                        'min' => 0,
                        'max' => 60,
                    ],
                ],
                'default'    => [
                    'unit' => 'px',
                    'size' => 10,
                ],
            ]
        );
        $this->add_control(
            'input_size',
            [
                'label'   => __('Input size', 'sb-elementor'),
                'type'    => \Elementor\Controls_Manager::SELECT,
                'options' => [
                    'elementor-size-xs' => __('Extra small', 'sb-elementor'),
                    'elementor-size-sm' => __('Small', 'sb-elementor'),
                    'elementor-size-md' => __('Medium', 'sb-elementor'),
                    'elementor-size-lg' => __('Large', 'sb-elementor'),
                    'elementor-size-xl' => __('Extra Large', 'sb-elementor'),
                ],
                'default' => 'elementor-size-xs',
            ]
        );
        $this->end_controls_section();
    }

    protected function render()
    {
        $settings = $this->get_settings_for_display();
        $check    = \Elementor\Plugin::$instance->editor->is_edit_mode() || !is_user_logged_in() || \Elementor\Plugin::$instance->preview->is_preview_mode() || current_user_can('edit_pages');
        ob_start(); ?>
        <?php if ($check): ?>
        <form method="post" class="custom_register_form" id="custom_register_form"
              data-nonce="<?php echo wp_create_nonce('wp_rest'); ?>" data-base="<?php echo get_home_url(); ?>"
              style="width: <?php echo $settings['form_width']['size'] . $settings['form_width']['unit']; ?>">
            <input type="hidden" value="<?php echo $settings['auto_username']; ?>" name="auto_username">
            <div class="elementor-form-fields-wrapper">
                <?php if ($settings['display_name'] === 'yes'): ?>
                    <?php echo $this->renderTextField('username', $settings['display_name_label'], true, $settings['input_size'], $settings['show_labels'], $settings['row_gap']); ?>
                <?php endif; ?>
                <?php echo $this->renderTextField('email', 'E-mail', true, $settings['input_size'], $settings['show_labels'], $settings['row_gap'], 'email'); ?>
                <?php if ($settings['show_password'] === 'yes'): ?>
                    <?php echo $this->renderTextField('password', $settings['password_label'], true, $settings['input_size'], $settings['show_labels'], $settings['row_gap']); ?>
                    <?php echo $this->renderTextField('re-password', $settings['re_password_label'], true, $settings['input_size'], $settings['show_labels'], $settings['row_gap']); ?>
                <?php endif; ?>
                <?php if ($settings['show_newsletter'] === 'yes'): ?>
                    <?php echo $this->renderCheckbox('newsletter', $settings['newsletter_label'], false, '', $settings['row_gap']); ?>
                <?php endif; ?>
                <?php
                $dmca    = $settings['dmca_url'];
                $terms   = $settings['terms_url'];
                $privacy = $settings['privacy_url'];
                ?>
                <?php if ($dmca['url'] || $terms['url'] || $privacy['url']): ?>
                    <?php
                    $string = 'I understand and agree with the ';
                    ?>
                    <?php
                    if ($dmca['url'] && $terms['url'] && $privacy['url']) {
                        $string .= '<a href="' . $dmca['url'] . '">' . __('DMCA', 'sb-elementor') . '</a>, <a href="' . $terms['url'] . '">' . __('Terms & Conditions', 'sb-elementor') . '</a> ' . __('and', 'sb-elementor') . ' <a href="' . $privacy['url'] . '">' . __('Privacy Policy', 'sb-elementor') . '</a>';
                    } elseif ($dmca['url'] && $terms['url'] && !$privacy['url']) {
                        $string .= '<a href="' . $dmca['url'] . '">' . __('DMCA', 'sb-elementor') . '</a> ' . __('and', 'sb-elementor') . ' <a href="' . $terms['url'] . '">' . __('Terms & Conditions', 'sb-elementor') . '</a>';
                    } elseif ($dmca['url'] && !$terms['url'] && $privacy['url']) {
                        $string .= '<a href="' . $dmca['url'] . '">' . __('DMCA', 'sb-elementor') . '</a> ' . __('and', 'sb-elementor') . ' <a href="' . $privacy['url'] . '">' . __('Privacy Policy', 'sb-elementor') . '</a>';
                    } elseif (!$dmca['url'] && $terms['url'] && $privacy['url']) {
                        $string .= '<a href="' . $terms['url'] . '">' . __('Terms & Conditions', 'sb-elementor') . '</a> ' . __('and', 'sb-elementor') . ' <a href="' . $privacy['url'] . '">' . __('Privacy Policy', 'sb-elementor') . '</a>';
                    } elseif ($dmca['url'] && !$terms['url'] && !$privacy['url']) {
                        $string .= '<a href="' . $dmca['url'] . '">' . __('DMCA', 'sb-elementor') . '</a>';
                    } elseif (!$dmca['url'] && $terms['url'] && !$privacy['url']) {
                        $string .= '<a href="' . $terms['url'] . '">' . __('Terms & Conditions', 'sb-elementor') . '</a>';
                    } elseif (!$dmca['url'] && !$terms['url'] && $privacy['url']) {
                        $string .= '<a href="' . $privacy['url'] . '">' . __('Privacy Policy', 'sb-elementor') . '</a>';
                    }
                    ?>
                    <?php echo $this->renderCheckbox('terms', $string, true, '', $settings['row_gap']); ?>
                <?php endif; ?>
                <?php if ($settings['enable_free_product'] === 'yes' && class_exists('WooCommerce') && $settings['enable_free_product_text'] === 'yes'): ?>
                    <div class="form-group">
                        <h2><?php echo $settings['free_product_title']; ?></h2>
                        <p><?php echo $settings['free_product_info']; ?></p>
                    </div>
                <?php endif; ?>
                <?php if ($settings['enable_woo_fields'] && class_exists('WooCommerce')): ?>
                    <?php
                    $countries_obj = new WC_Countries();
                    $countries     = $countries_obj->__get('countries');
                    ?>
                    <?php echo $this->renderTextField('firstname', 'Firstname', false, $settings['input_size'], $settings['show_labels'], $settings['row_gap']); ?>
                    <?php echo $this->renderTextField('lastname', 'Lastname', false, $settings['input_size'], $settings['show_labels'], $settings['row_gap']); ?>
                    <?php if ($settings['company_show'] === 'yes'): ?>
                        <?php echo $this->renderTextField('company_name', 'Company Name', false, $settings['input_size'], $settings['show_labels'], $settings['row_gap']); ?>
                    <?php endif; ?>
                    <?php echo $this->renderSelect('country', 'Country', false, $settings['input_size'], $countries, $settings['show_labels'], $settings['row_gap']); ?>
                    <?php echo $this->renderTextField('address1', 'Street Address', false, $settings['input_size'], $settings['show_labels'], $settings['row_gap']); ?>
                    <?php if ($settings['address2_show'] === 'yes'): ?>
                        <?php echo $this->renderTextField('address2', 'Street Address 2', false, $settings['input_size'], $settings['show_labels'], $settings['row_gap']); ?>
                    <?php endif; ?>
                    <?php echo $this->renderTextField('city', 'City / Town', false, $settings['input_size'], $settings['show_labels'], $settings['row_gap']); ?>
                    <?php echo $this->renderTextField('state', 'State / Province / Region', false, $settings['input_size'], $settings['show_labels'], $settings['row_gap']); ?>
                    <?php echo $this->renderTextField('zip', 'Zip / Postal Code', false, $settings['input_size'], $settings['show_labels'], $settings['row_gap']); ?>
                    <?php if ($settings['phone_show'] === 'yes'): ?>
                        <?php echo $this->renderTextField('phone', 'Mobile Phone Number', false, $settings['input_size'], $settings['show_labels'], $settings['row_gap']); ?>
                    <?php endif; ?>
                <?php endif; ?>
                <?php if($settings['products']): ?>
                    <span style="display: block; width: 100%; flex-basis: 100%;"><b><?php echo __('Select your free gift.'); ?></b></span>
                    <?php $pOptions['none'] = '-- Choose your products --- '; ?>
                    <div style="display: flex; justify-content: space-between; flex-wrap: wrap; width: 100%">
                    <?php foreach ($settings['products'] as $pid): ?>
                        <?php $product = get_post($pid); ?>
                        <div class="custom-parent">
                            <figure>
                                <?php $id = get_post_thumbnail_id($pid); ?>
                                <?php $img = wp_get_attachment_image_src($id, 'medium'); ?>
                                <img src="<?php echo $img[0]; ?>">
                            </figure>
                            <label class="button button-primary" style="margin-left: auto; margin-right: auto; text-align: center"
                                   for="product-<?php echo $product->ID; ?>">
                                <?php echo __('Select'); ?>
                            </label>
                            <input type="radio" value="<?php echo $product->ID; ?>" id="product-<?php echo $product->ID; ?>"
                                   name="product" style="position: absolute; opacity: 0; width: 0; height: 0;" class="custom-radio-check">
                        </div>
                <?php $pOptions[$product->ID] = $product->post_title; ?>
                    <?php endforeach; ?>
                    </div>
                <?php endif; ?>
                <div id="errors" style="flex-basis: 100%; width: 100%; color: red;"></div>
                <div class="elementor-field-group elementor-column elementor-field-type-submit elementor-col-100">
                    <button type="submit" class="elementor-size-sm elementor-button">
                        <span class="elementor-button-text">Register</span>
                    </button>
                </div>
            </div>
        </form>
        <div id="success-register" style="display: none;"></div>
    <?php else: ?>
        <?php $user = wp_get_current_user(); ?>
        <p><?php echo __('You are logged in as '); ?> <?php echo $user->display_name; ?> <a href="<?php echo wp_logout_url( get_permalink() ); ?>">Logout</a></p>
    <?php endif; ?>
        <?php echo ob_get_clean();
    }

    private function renderTextField($id, $name, $required, $size, $showLabel, $gap, $type = 'text')
    {
        ob_start(); ?>
        <div class="elementor-field-type-text elementor-field-group elementor-column elementor-col-100 <?php echo $required ? 'elementor-field-required' : ''; ?>"
             style="margin-bottom: <?php echo $gap['size']; ?>px">
            <?php if ($showLabel === 'yes'): ?>
                <label for="<?php echo $id; ?>"><?php echo $name; ?></label>
            <?php endif; ?>
            <input size="1" type="<?php echo $type; ?>" name="<?php echo $id; ?>" id="<?php echo $id; ?>"
                   placeholder="<?php echo $name; ?>"
                   class="elementor-field elementor-field-textual <?php echo $size; ?>">
        </div>
        <?php
        return ob_get_clean();
    }

    private function renderCheckbox($id, $name, $required, $size, $gap)
    {
        ob_start(); ?>
        <div class="elementor-field-type-checkbox elementor-field-group elementor-column elementor-col-100"
             style="margin-bottom: <?php echo $gap['size']; ?>px">
            <label for="<?php echo $id; ?>">
                <input type="checkbox" id="<?php echo $id; ?>" name="<?php echo $id; ?>"
                       value="1" <?php echo $required ? 'required' : ''; ?>>
                <?php echo $name; ?>
            </label>
        </div>
        <?php
        return ob_get_clean();
    }

    private function renderSelect($id, $name, $required, $size, $options, $showLabel, $gap)
    {
        ob_start(); ?>
        <div class="elementor-field-type-select elementor-field-group elementor-column elementor-field-group-field_1 elementor-col-100"
             style="margin-bottom: <?php echo $gap['size']; ?>px">
            <?php if ($showLabel === 'yes'): ?>
                <label for="<?php echo $id; ?>"><?php echo $name; ?></label>
            <?php endif; ?>
            <div class="elementor-field elementor-select-wrapper ">
                <select id="<?php echo $id; ?>" name="<?php echo $id; ?>"
                        class="elementor-field-textual <?php echo $size; ?>" <?php echo $required ? 'required' : ''; ?>>
                    <option value=""><?php echo __('-- Choose --'); ?></option>
                    <?php foreach ($options as $key => $value): ?>
                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
        <?php return ob_get_clean();
    }

    private function getAllProducts()
    {
        $products = [];
        $args = [
                'post_type' => 'product',
                'posts_per_page' => -1,
                'post_status' => 'publish'
        ];
        $query = new WP_Query($args);
        if($query->have_posts()){
            /**
             * @var WP_Post $product
             */
            foreach ($query->posts as $product){
                $products[$product->ID] = $product->post_title;
            }
        }
        return $products;
    }
}